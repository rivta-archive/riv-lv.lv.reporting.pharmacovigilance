* Instruktioner för hur man kommer igång med bitbucket http://rivta.se/bitbucket/git-sourcetree.html
# [Till wiki](https://bitbucket.org/rivta-domains/best-practice/wiki)
# [Till ärendehantering](https://bitbucket.org/rivta-domains/riv-lv.lv.reporting.pharmacovigilance/issues)

This domain uses xspec to verify the schematron rules found in test-suite/ProcessIndividualCaseSafetyReport/constraints.xml
More information about xspec can be found at [xml.com](https://www.xml.com/articles/2017/03/15/what-xspec/) and [xspec](https://github.com/xspec/xspec)
