<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/testsuite">
		<html>
			<head>
		
			</head>
			<body>
				<h1>TK-testsvit för <xsl:value-of select="id"/></h1>
				
				<h2>Om testsviten</h2>
				<p>Detta dokument beskriver testsviten för <xsl:value-of select="id"/>. Testsviten innehåller ett antal testfall som kan användas för att verifiera implementationen innan integrationen med den nationella tjänsteplattformen.<br/>
				Testsviten använder SoapUI för att verifiera implementationen. Dokumentation om SoapUI hittas här: <a target="blank" href="https://www.soapui.org/getting-started/introduction.html">www.soapui.org</a>.<br/>
				Klicka på <a target="blank" href="https://www.soapui.org/downloads/soapui.html">den här länken</a> för att ladda hem en gratisversion av SoapUI. Installera enligt anvisning.</p>
				
				<h2>Förberedelser</h2>
				<p>
					<ul>
						<li>Gå till mappen <b>test-suite</b> i release-paketet</li>
						<li>Kopiera filen <b>‘soapui-support-N.N.NN.jar’</b> ('N.N.NN' är versionsnummer) till mappen <b>/bin/ext</b> där Soap-UI är installerat (leta efter mappen <b>/Program Files/Smartbear</b>)</li>
						<li>Öppna SoapUI och importera SoapUI-projektet (<b>test-suite/<xsl:value-of select="contractName"/>/<xsl:value-of select="contractName"/>-soapui-project.xml</b>) (välj ‘Import Project’ från menyn 'File')</li>
						<li>Om din webservice kräver ett SSL-certifikat, kan detta konfigureras via 'Preferences' (via menyn 'File').  
						I fönstret för inställningar, gå till fliken 'SSL Settings' och importera ditt certifkat under 'Keystore'</li>
						<li>Uppdatera <i>data.xml</i> så att den matchar den testdata du har i ditt system. Om du inte har testdata som passar så behöver detta läggas in i källsystemet (se nedan för instruktioner)</li>
						<li>Du borde nu kunna köra testfallen i SoapUI</li>
					</ul>
				</p>
				
				<h2>Testdata i <i>data.xml</i></h2>
				<p>
					Innan man kör testfallen i SoapUI så måste den data som skickas med i anropen anpassas utifrån det system som man vill testa. Detta görs genom att ändra i filen <i>data.xml</i> enligt nedan.<br/>
					<br/>
					Filen är i XML-format och i början finns en sektion som heter "globaldata". Här anger man den konfiguration som kommer att användas av alla testfall.<br/>
					Varje element i "globaldata" kan omdefinieras för ett specifikt testfall vid behov. Följande element är globala:
					<ul>
						<xsl:for-each select="globaldata/*">
							<li>
								<xsl:value-of select="name()"/>
							</li>
						</xsl:for-each>
					</ul>
					Globaldata innehåller ett antal konfigurationsparametrar för loggning: <br/>
                    logTestData: Sätts till true/false beroende på om loggning ska utföras eller ej. <b> Observera att patientdata kan lagras vid påslagen loggning.</b> <br/>
                    logTestDataPath: Sökvägen till den katalog där loggfilerna sparas, måste vara en katalog som användaren har rättighet att skriva i. <br/>
                    logTestDataFilesAllowed: Max antal filer som sparas. Det blir en fil för varje testfall som körs. När max antal filer har uppnåtts tas de äldsta bort så nya kan sparas.<br/>
				</p>
				
				<h2>Allmänna tips</h2>
				<p>
					<ul>
						<li>Kör ett testfall i taget och verifiera resultatet. Man kan också köra en hel testsvit (t.ex. "1 Basic tests") för att köra igenom alla testfall i just den sviten.<br/>
						Om du gör någon ändring som ska påverka ett specifikt testfall, kan man efter att ha verifierat just det testfallet köra genom hela sviten för att snabbt se att det "hänger ihop".</li>
						<li>Eventuella felmeddelanden skrivs ut dels till fönstret för testfallet och dels i sektionen "assertions" i anrops-fönstret.</li>
					</ul>
				</p>
				
				<h2>Beskrivning av testfallen</h2>
				<p>De parameterar man anger för ett specifikt testfall kompletterar och/eller omdefinierar de parameterar som anges i "globaldata".<br/>
				Det betyder att både parametrar från "globaldata" och det specifika testfallets sektion i filen används för det aktuella testfallet.<br/>
				OBS! Om en parameter med samma namn definieras både i "globaldata" och specifikt för testfallet, så kommer värdet från testfalls-sektionen att användas.<br/>
				Ett exempel kan vara "patientId". Denna definieras i "globaldata", eftersom det är troligt att det mesta av testdatan kommer att röra samma patient.<br/>
				Men för vissa testfall vill man kunna använda en annan patient och för dessa testfall definierar man detta genom att ta bort kommentars-markeringen runt parametern "patientId" i testfallets sektion.<br/>
				Glöm inte att spara <i>data.xml</i> efter att du har ändrat i den.</p>
				<ul style="list-style-type:none">
					<xsl:for-each select="testcase|section">
						<h3>
							<li>
								<xsl:value-of select="@id"/>
							</li>
						</h3>
						<p>
							<xsl:choose>
								<xsl:when test="starts-with(@id, '1.1 ')">Filtrering. Verifierar att resultatet endast innehåller poster för given patient.</xsl:when>
								<xsl:when test="starts-with(@id, '1.3 ')">Filtrering. Verifierar att resultatet endast innehåller poster för given vårdenhet.</xsl:when>
								<xsl:when test="starts-with(@id, '1.4 ')">Filtrering. Verifierar att resultatet endast innehåller poster för givet källsystem.</xsl:when>
								<xsl:when test="starts-with(@id, '1.5 ')">Filtrering. Verifierar att resultatet endast innehåller poster för given vårdkontakt.</xsl:when>
								<xsl:when test="starts-with(@id, '1.6 ')">Filtrering. Verifierar att resultatet endast innehåller poster för given vårdgivare.</xsl:when>
								<xsl:when test="starts-with(@id, '1.7 ')">Filtrering. Verifierar att tjänsteproducenten kan filtrera resultatet baserat på HTTP-headern "x-rivta-original-serviceconsumer-hsaid".<br/>
									Responsen ska bara innehålla poster med fördefinierade dokument-id:n. Ges nedan i en kommaseparerad lista.<br/>
									Testfallet är ej applicerbart för system som inte implementerat denna typ av filtrering.</xsl:when>
								<xsl:when test="starts-with(@id, '1.8 ')">Verifierar att resultatet är ett SOAP Exception. Detta testfall kräver att tjänsteproducenten skapar förutsättningar för ett internt fel att uppstå.<br/>
									Exempel kan vara att man stänger av kopplingen mot databas.</xsl:when>
								<xsl:when test="starts-with(@id, '2.1 ')">Verifierar att
									<ul>
										<li>Header-attributet "Content-type" har, om attributet finns, en teckenuppsättning som är satt till UTF-8 eller UTF-16</li>
										<li>Attributet "XML Prolog" har, om attributet finns, en teckenuppsättning som är satt till UTF-8 eller UTF-16</li>
										<li>Om båda attributen finns så ska de vara lika</li>
									</ul></xsl:when>
								<xsl:when test="starts-with(@id, '2.2 ')">Verifierar att responsen innehåller en sträng med specialtecken.<br/>
									Denna sträng behöver läggas upp på en post i källsystemet och bör innehålla så många specialtecken som möjligt.</xsl:when>
								<xsl:when test="starts-with(@id, '4.1 ')">Verifierar att en av de returnerade posterna innehåller elementet approvedForPatient satt till "true".</xsl:when>
								<xsl:when test="starts-with(@id, '4.2 ')">Verifierar att en av de returnerade posterna innehåller elementet approvedForPatient satt till "false".</xsl:when>
								<xsl:when test="starts-with(@id, '5.1 signed')">Verifierar att systemet kan producera en signerad post.</xsl:when>
								<xsl:when test="starts-with(@id, '5.2 unsigned')">Verifierar att systemet kan producera en osignerad post.</xsl:when>
								<xsl:when test="starts-with(@id, '5.3 locked')">Verifierar att systemet kan producera en låst post. Posten ska alltså inte signeras utan låsas pga timeout.</xsl:when>
								<xsl:when test="starts-with(@id, 'Concurrency')">Verifierar att systemet kan hantera samtidiga anrop utan sammanblandning av data.</xsl:when>
								<xsl:when test="element">Verifierar att systemet kan producera elementet <b><xsl:value-of select="element"/></b>.</xsl:when>
								<xsl:otherwise>
									<xsl:copy-of select="description"/>
								</xsl:otherwise>							
							</xsl:choose>							
						</p>
						<xsl:if test="data/*">
							<b>Testfalls-specifika parametrar</b>
						</xsl:if>
						<ul>
							<xsl:for-each select="data/*">
								<li>
									<xsl:value-of select="name()"/>
								</li>
							</xsl:for-each>
						</ul>
					</xsl:for-each>
				</ul>
				<br/>
				<br/>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet> 